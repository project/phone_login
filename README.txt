INFORMATION FOR DEVELOPERS

Set variable 'phone_login_random_start' and 'phone_login_random_end' to set the
varify code random scope.
Set variable 'phone_login_phone_regexp' set the phone number validate regexp,
default is '^1\d{10,10}$'.

Implement hook_phone_login_sms_alter to alter sms message content.
